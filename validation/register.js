const validator = require('validator')

module.exports = function validateRegisterInput(data) {

  let errors = {}

  if (!validator.isLenght(data.userName, {
      min: 2,
      max: 30
    })) {
    errors.userName = 'نام کاربری  بین دو تا سی کاراکتر باشد'
  }

  return {
    errors,
    isValid: errors
  }



}