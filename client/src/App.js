import React, { Component } from 'react';
import './App.css';
import TopHeader from './components/layout/TopHeader'
import MainHeader from './components/layout/MainHeader'

class App extends Component {
  render() {
    return (
      <div className="App">
        <TopHeader />
        <MainHeader />
      </div>
    );
  }
}

export default App;
