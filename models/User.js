const mongoose = require('mongoose')
const Schema = mongoose.Schema


//create Schema

const userSchema = new Schema({
  userName: {
    type: String,
    required: true
  },
  phoneNumber: {
    type: String,
    required: true
  },
  email: {
    type: String
  },
  password: {
    type: String,
    required: true
  },
  avatar: {
    type: String,
  },
  date: {

    type: Date,
    default: Date.now
  }
})


module.exports = User = mongoose.model('users', userSchema)